function y = lcg(n, min, max)
  a = 13;
  c = 53;
  x = rand()*max;
  x=ceil(x);
  
  for i=1:n
    z= a*x+c;
    y(i)=(ceil(mod (z,max)));
    if y(i) < min;
      y(i) = y(i) + min;
    end
    x=y(i);
  end