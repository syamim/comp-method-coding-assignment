function s =rdday (&a)
  if (a < 21)
    s=l;
  elseif (a < 51)
    s=2;
  elseif (a < 66)
    s=3;
  elseif (a < 86)
    s=4;
  else
    s=5;
  end