function customer = customerTable(randomizer, &numberCustomer, totalTicket, moviePrice)
  arrival = 0;
  customer = struct('rdservice', cell(1, numberCustomer), 'service', cell(1, numberCustomer), 'rdinterval', cell(1, numberCustomer));

  for (i=1:numberCustomer);
  totalTicketDay=sum(totalTicket);
  if(sum(totalTicketDay)==0)
    numberCustomer=i-1;
    break;
  end

  random=feval(randomizer, 5, 0, 100);
  customer(1, i).rdservice=random(1);
  customer(1, i).service=rdservice(customer(1, i).rdservice);
  customer(1, i).rdinterval=random(2);
  customer(1, i).interval=rdinterarr(customer(1,i).rdinterval);
  customer(1, i).arrivalTime=arrival;
  arrival=arrival+customer(1, i).interval;;

  while (true);
    Random=feval (randomizer, 5,0,100);
    customer (1,i).rdDay=random (1);
    customer (1,i).day=rdday(customer (1, i).rdDay);
    if (totalTicketDay(customer (1,i).day)==0)
      continue;
    end
    
    customer(1,i).rdType=random (2);
    customer(1,i).type=rdmovie(customer(1,i).rdDay);
    if (strcmp (customer (1,i).type, 'Bumble' ))
      if (totalTicket(1,customer(1,i).day)~=0)
        type = 1;
      else
        continue;
      end
    end
    if (strcmp (customer (1,i).type, 'Frozen'))
      if (totalTicket(2,customer (1,i).day)~=0)
        type = 2;
      else; % elseran -- entah la nak
        continue;
      end
    end
    if (strcmp (customer (1,i).type, 'Take Point'))
      if (totalTicket(3, customer (1,i).day) ~=0)
        type = 3;
      else
        continue;
      end
    end
    if (strcmp (customer (1,i).type, 'Aquaman'))
      if (totalTicket(4, customer (1,i).day) ~=0)
        type = 4;
      else
        continue;
      end
    end
    if (strcmp (customer (1,i).type, 'Robin Hood'))
      if (totalTicket(5, customer (1,i).day)~=0)
        type = 5;
      else
        continue;
      end
    end

    max = 10;
    if (totalTicket(type, customer(1, i).day) < max)
      max = totalTicket(type, customer(1, i).day);
    end
    customer(1, i).numberTicket = feval(randomizerm, 5, 1, max)(1);
    if (totalTicket(type, customer(1,i).day)-customer(1,i).numberTicket >= 0)
      break;
    end
  end
  customer(1, i).totalAmountPaid = customer(1, i).numberTicket*moviePrice(type);
  totalTicket(type, customer(1,i).day) = totalTicket(type, customer(1, i).day) - customer(1, i).numberTicket;
