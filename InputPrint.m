function InputPrint(&totalTicket, &serviceTable, &interarrTable, &dayTable, &movieName, &ticketTable, &moviePrice)
  printf('----------------------------------------------------------\n');
  printf('| Day | Bumble | Frozen | Take Point | Aqua | Robin Hood |\n');
  printf('----------------------------------------------------------\n');
  for (i=1:5);
    printf('| %4d | %6d | %6d | %10d | %4d | %10d |\n', i, totalTicket(1, i), totalTicket(2, i), totalTicket(3, i), totalTicket(4, i), totalTicket(5, i));
  end;
  printf('----------------------------------------------------------\n');
  
  printf('-------------------------------------------------\n');
  printf('| Service Time | Probability | CDF |    Range   |\n');
  printf('-------------------------------------------------\n');
  for (i=3:7);
    printf('| %12d | %11.2f | %3.2f | %5d -%4d |\n', i, serviceTable(1, i-2), serviceTable(2, i-2), serviceTable(3, i-2), serviceTable(4, i-2));
  end;
  printf('-------------------------------------------------\n');

  printf('---------------------------------------------------\n');
  printf('| Inter-arrival Time | Probability | CDF | Range  |\n');
  printf('---------------------------------------------------\n');
  for(i=1:5);
    printf('| %18d | %11.2f | %3.2f | %2d -%3d |\n', i, interarrTable(1, i), interarrTable(2, i), interarrTable(3, i), interarrTable(4, i));
  end;
  printf('---------------------------------------------------\n');

  printf('------------------------------------\n');
  printf('| Day | Probability | CDF |  Range |\n');
  printf('------------------------------------\n');
  for(i=1:5);
    printf('| %3d | %11.2f | %3.2f | %2d -%3d |\n', i, dayTable(1, i), dayTable(2, i), dayTable(3, i), dayTable(4, i));
  end;
  printf('------------------------------------\n');

  printf('---------------------------------------------------\n');
  printf('| Ticket Type | Probability | CDF | Range | Price |\n');
  printf('---------------------------------------------------\n');
  for(i=1:5);
    printf('| %11d | %11.2f | %3.2f | %2d -%3d | %5d |\n', movieName(i, 1:10), ticketTable(1, i), ticketTable(2, i), ticketTable(3, i), ticketTable(4, i), moviePrice(1));
  end;
  printf('---------------------------------------------------\n');