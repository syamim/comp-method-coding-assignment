function s = rdmovie(&a)
  if (a < 21)
    s='Bumble';
  elseif (a < 51)
    s='Frozen';  
  elseif (a < 76)
    s='Take Point';
  elseif (a < 86)
    s='Aquaman';
  else
    s='Robin';
  end
    