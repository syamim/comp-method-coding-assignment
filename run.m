function y = run(n, min, max)
  x = rand(1, n);
  z = min + (max-min) * x;
  y = ceil(z);
  end