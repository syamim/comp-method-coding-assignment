function counter = counterTable(&customer, numberCustomer, numberCounter, numberVipCounter, &afterTotalTicket, custType)
  counter = struct('noCustomer', cell(numberCounter+numberVipCounter, numberCustomer), 'service', cell(numberCounter+numberVipCounter, numberCustomer));
  for(i = 1:numberCounter+numberVipCounter)
    counter(i, 1).timeServiceBegin = 0;
  end
  beforeTotalTicket = afterTotalTicket;
  a = ones(i, numberCounter+numberVipCounter);
  lastService = 0;

  for (i = 1:numberCustomer)
    if(custType(i)==1)
      smallest = 1;
      if(numberCounter>1)
        for(j=1:(numberCounter-1))
          if (counter(j+1,a(j+1)).timeServiceBegin < counter(smallest, a(smallest)).timeServiceBegin)
            smallest=j+1;
          end;
        end;
      end;
    end;

    if (custType(i)==2)
      smallest = numberCounter+1;
      if(numberVipCounter>1)
        for(j=numberCounter+1:numberCounter+numberVipCounter-1)
          if(counter(j+1, a(j+1)).timeServiceBegin < counter(smallest, a(smallest)).timeServiceBegin)
            smallest = j+1;
          end;
        end;
      end;
    end;

    counter(smallest, a(smallest)).noCustomer = i;
    counter(smallest, a(smallest)).service = customer(i).service;
    if(counter(smallest, a(smallest)).timeServiceBegin < customer(i).arrivalTime)
      counter(smallest, a(smallest)).waitingTime=0;
      counter(smallest, a(smallest)).timeServiceBegin = customer(i).arrivalTime;
    else
      counter(smallest, a(smallest)).waitingTime = counter(smallest, a(smallest)).timeServiceBegin - customer(i).arrivalTime;
    end;

    counter(smallest, a(smallest)).timeServiceEnd = counter(smallest, a(smallest)).timeServiceBegin + counter(smallest, a(smallest)).service;
    counter(smallest, a(smallest)).timeSpend = counter(smallest, a(smallest)).service + counter(smallest, a(smallest)).waitingTime; 
    a(smallest) = a(smallest)+1;
    counter(smallest, a(smallest)).timeServiceBegin = counter(smallest, a(smallest-1)).timeServiceEnd;
    if (counter(smallest, a(smallest-1)).timeServiceEnd > lastService)
      lastService = counter(smallest, a(smallest-1)).timeServiceEnd;
    end;
  end;

  for ( i = numberCounter + 1:numberCounter+numberVipCounter)
    printf('Counter %1d, ', i);
    if (i == numberCounter+numberVipCounter)
      printf(' are only for VIP customer\n');
    end;
  end;

  printf('Minute 0000: Counter 1 and %1d are in operation.\n', numberCounter+!);
  a = ones(1, numberCounter+numberVipCounter);
  b = ones(1, numberCounter+numberVipCounter);
  arrivalCustomer = 1;
  time = 0;
  for (i = 0:lastService)
    if(arrivalCustomer <= numberCustomer)
      if(customer(1, arrivalCustomer).arrivalTime == i)
        printf('Minute %04d: Arrival of ', customer(1, arrivalCustomer).arrivalTime);
        if (arrivalCustomer == 1)
          printf( ' first ');
        elseif (arrivalCustomer == 2)
          printf( ' second ');
        elseif (arrivalCustomer == 3)
          printf( ' third ');
        else
          printf('%4dth', arrivalCustomer);
        end;
        if (custType(arrivalCustomer) == 1)
          printf('Guest ');
        else
          printf('VIP ');
        end;

        printf('Customer purchase %1d tickets of d%1s.\n', customer(1, arrivalCustomer).numberTicket, customer(1, arrivalCustomer).type);
        arrivalCustomer = arrivalCustomer+1;
      end;

      for (j= 1:numberCounter+numberVipCounter)
        if(counter(j,b(j)).timeServiceEnd == i)
          printf('Minute %04d: Departure of ', counter(j,b(j)).timeServiceEnd);
          if(counter(j, b(j)).noCustomer ==1)
            printf(' first ');
          elseif(counter(j, b(i)).noCustomer ==2)
            printf(' second ');
          elseif(counter(j, b(i)).noCustomer ==3)
            printf(' third ');
          else
            printf('%4dth ', counter(j, b(j)).noCustomer);
          end;
          if (custType(counter(j, b(j)).noCustomer) == 1)
            printf(' first ');
          elseif (custType(counter(j, b(j)).noCustomer) == 2)
            printf(' second ');
          elseif (custType(counter(j, b(j)).noCustomer) == 3)
            printf(' third ');
          else
            printf(' %04dth ', counter(j, b(j)).noCustomer);
          end;
          
          if (custType(counter(j, b(j)).noCustomer) == 1)
            printf('Guest ');
          else
            printf('VIP ');
          end;
          printf('customer where the Counter Number is %1d.\n');
          b(j) = b(j)+1;
        end;

        if (isEmpty(counter(j, a(j)).timeServiceBegin))
          continue;
        end;
        
        if (counter(j, a(j)).timeServiceBegin == i)
          if (a(j) == 1 & ~i==0 & ~j==numberCounter+1)
            printf('Minute %04d: Counter %1d start operation.\n', counter(j, a(j)).timeServiceBegin, j);
          end;
          printf('Minute %04d: Service for ', counter(j ,a(j)).timeServiceBegin);
          if (counter(j, a(j)).noCustomer == 1)
            printf( ' first ');
          elseif (counter(j, a(j)).noCustomer == 2)
            printf( ' second ');
          elseif (counter(j, a(j)).noCustomer == 3)
            printf( ' third ');
          else
            printf('%4dth ',counter(j, a(j)).noCustomer);
          end;

          if(custType(counter(j, a(j)).noCustomer) == 1)
            printf( 'Guest ');
          else
            printf('VIP ');
          end;

          printf('customer started where Counter Number is %1d.\n', j);
          if (a(j)>1)
            if(counter(j,a(j)).timeServiceBegin-counter(j,a(j)-1).timeServiceEnd > 0)
              printf('Counter %1d had take %1d minutes to rest.\n', j, (counter(j,a(j)).timeServiceBegin-counter(j, a(j)-1).timeServiceEnd));
            end;
          end;
          
          if (strcmp(customer(1, counter(j, a(j)).noCustomer).type, 'Bumble'))
            type = 1;
          end;
          if (strcmp(customer(1, counter(j, a(j)).noCustomer).type, 'Frozen'))
            type = 2;
          end;
          if (strcmp(customer(1, counter(j, a(j)).noCustomer).type, 'Take Point'))
            type = 3;
          end;
          if (strcmp(customer(1, counter(j, a(j)).noCustomer).type, 'Aquaman'))
            type = 4;
          end;
          if (strcmp(customer(1, counter(j, a(j)).noCustomer).type, 'Robin Hood'))
            type = 5;
          end;
          afterTotalTicket(type, customer(1, counter(j, a(j)).noCustomer).day) = afterTotalTicket(type, customer(1, counter(j,a(j)).noCustomer).day) - customer(1, counter(j, a(j)-1).noCustomer).day
          if ((afterTotalTicket(type, customer(1, counter(j, a(j)).noCustomer).day)) == 0)
            printf('Ticket type %1s of the %1d days is sold out\n', customer(1, counter(j, a(j)).noCustomer).type, customer(1, counter(j, a(j)).noCustomer).day);
          end;
          a(j) = a(j)+1;
        end;

        time = time + 1;
        if (time == 60 | i == (lastService-1))
          fastest = 1;
          for(i = 1:4)
            totalBefore1 = 0;
            totalAfter1 = 0;
            totalBefore2 = 0;
            totalAfter2 = 0;
            for (j = 1:5)
              totalBefore1 = totalBefore1 +  beforeTotalTicket(fastest, j);
              totalAfter1 = totalAfter1 + afterTotalTicket(fastest, j);
              totalBefore2 = totalBefore2 +  beforeTotalTicket(i+1, j);
              totalAfter2 = totalAfter2 + afterTotalTicket(i+1, j);
            end;
            percentage1 = (totalBefore1 - totalAfter1)/double(totalBefore1)*100;
            percentage2 = (totalBefore2 - totalAfter2)/double(totalBefore2)*100;
            if (percentage1 < percentage2)
              fastest = i +1;
            end;
          end;

          if (fastest == 1)
            typeName = 'Bumble';
          end;
          if (fastest == 2)
            typeName = 'Frozen';
          end;
          if (fastest == 3)
            typeName = 'Take Point';
          end;
          if (fastest == 4)
            typeName = 'Aquaman';
          end;
          if (fastest == 5)
            typeName = 'Robin Hood';
          end;
          printf('Ticket type %1s is selling fastest\n', typeName);
          time = 0;
          beforeTotalTicket = afterTotalTicket;
        end;
        if (i==lastService)
          for(j=1:numberCounter+numberVipCounter)
            printf('Minute %04d: Counter %1d closed\n', i, j);
          end;
        end;
      end;
    end;
  end;





