function print(&customer, customerNumber, &counter, numberCounter, numberVipCounter, &totalTicket)
  totalInterArr = 0;
  totalArr = 0;
  totalWaitTIme = 0
  totalWaitCust = 0;
  totalService = zeros(1, numberCounter+numberVipCounter);
  totalSales = zeros(1, numberCounter+numberVipCounter);
  totalTimeSpent = 0;
  custNumberCounter = zeros(numberCounter+numberVipCounter);
  
  printf('-----------------------------------------------------------------------------------------------------------------------\n');
  printf('|       RN for      | Interarrival | Arrival | RN for the | Day | RN for the | Movie |      Num of     | Total Amount |\n');
  printf('| Interarrival Time |     Time     |   Time  |     Day    |     |   Movie    |       | Ticket Purchase |   Paid (RM)  |\n');
  printf('-----------------------------------------------------------------------------------------------------------------------\n');
  for ( i = 1:customerNumber)
    // fuckkkkkk
    printf('| %17d | %12d | %7d | %10d | %3d | %10d | %5d | %15d | %12d |', i, customer(1, i).rdinterval, customer(1, i).interval, customer(1, i).arrivalTime, customer(1, i).rdDay, customer(1, i).day, customer(1, i).rdmovie,, customer(1, i). );
    // fuckkkkkk
    totalInterArr = totalInterArr + customer(1, i).interval;
    totalArr = totalArr + customer(1, i).arrivalTime;
  end;
  printf('-----------------------------------------------------------------------------------------------------------------------\n');

  for (i = 1:numberCounter+numberVipCounter)
    printf('\n Counter %2d ', i);
    if (i > numberCounter)
      printf(' for VIP only: \n');
    else
      printf(':\n');
    end;
    printf('---------------------------------------------------------------------------------- \n');
    printf('|    RN for    | Service | Time Service | Time Service |  Waiting |   Time Spend  |\n');
    printf('| Service Time |   Time  |     Begin    |      End     |   Time   | in the System |\n');
    printf('---------------------------------------------------------------------------------- \n');
    for (j = 1:customerNumber)
      if(isEmpty(counter(i, j).noCustomer))
        break;
      end;
      printf('| %12d | %7d | %12d | %12d | %8d | %13d |\n', counter(i, j).noCustomer, customer(counter(i, j).noCustomer).rdservice, counter(i, j).service, counter(i, j).timeServiceBegin, counter(i, j).timeServiceEnd, counter(i, j).waitingTime, counter(i, j).timeSpend);
      totalWaitTime = totalWaitTime + counter (i, j).waitingTime;
      if (counter(i, j),waitingTime > 0)
        totalWaitCust = totalWaitCust +1;
      end;
      totalService(i) = totalService(i)+counter(i, j).service;
      custNumberCounter(i) = custNumberCounter(i) + 1;
      totalTimeSpent = totalTimeSpent + counter(i, j).timeSpend;
      totalSales(i) = totalSales(i) + customer(1, counter(i, j).noCustomer).numberTicket;
    end;
    printf('---------------------------------------------------------------------------------- \n');

    printf('\n Remaining Ticker \n');
    printf('-------------------------------------------------------------\n');
    printf('| Day | Bumble | Frozen | Take Point | Aquaman | Robin Hood |\n');
    printf('-------------------------------------------------------------\n');
    for (i = 1:5)
      printf('| %3d | %6d | %6d | %10d | %7d | %10d |\n',i, totalTicket(1, i), totalTicket(2, i), totalTicket(3, i), totalTicket(4, i), totalTicket(5, i));
    end;
    printf('-------------------------------------------------------------\n');

    printf('Average Waiting TIme = %f\n',totalWaitTIme/customerNumber);
    printf('Average Inter-arrival Time = %f\n',totalInterArr/(customerNumber-1));
    printf('Average Arrival Time = %f\n',totalArr/(customerNumber-1));
    printf('Average Time Spent = %f\n',totalTimeSpent/customerNumber);
    printf('Probability that a customer has to wait in the queue = %f\n',totalWaitCust/customerNumber);
    for (i = 1:numberCounter+numberVipCounter)
      printf('Average Service Time for Counter %d = %f\n', i, totalService(i)/custNumberCounter(i));
    end;
    for (i = 1:numberCounter+numberVipCounter)
      printf('Average Sales for Counter %d = %f\n', i, totalSales(i)/custNumberCounter(i));
    end;
